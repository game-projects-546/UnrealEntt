﻿using System.IO;
using UnrealBuildTool;

public class EnttLibrary : ModuleRules
{
	public EnttLibrary(ReadOnlyTargetRules Target) : base(Target)
	{
		Type = ModuleType.External;
		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "entt"));
	}
}
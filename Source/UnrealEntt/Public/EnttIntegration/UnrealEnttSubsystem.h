// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnttWorld.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "entt.hpp"
#include "Containers/Ticker.h"

#include "UnrealEnttSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class UNREALENTT_API UUnrealEnttSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

protected:
	TUniquePtr<RadMag::FEnttWorld> World;
	FTickerDelegate OnTickDelegate;
	FDelegateHandle OnTickHandle;


public:
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;
	RadMag::FEnttWorld* GetECSWorld() const;

private:
	bool Tick(float DeltaTime) const;
};

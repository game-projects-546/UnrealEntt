﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "entt.hpp"

namespace RadMag
{
	using EEntity = entt::entity;

	enum class ESystemStatus
	{
		Repeat,
		Break,
		Failed
	};

	class FEnttData final
	{
	private:
		entt::registry ResourceStorage;
	public:
		entt::registry Registry;


		template <typename... Comp, template<typename...> typename List>
		auto Create(List<Comp...>) -> entt::entity
		{
			const auto Entity = Registry.create();
			(Registry.emplace<Comp>(Entity), ...);
			return Entity;
		}

		template <typename... Comp, typename... Exclude, template<typename...> typename List>
		auto GetView(List<Comp...>, entt::exclude_t<Exclude...> ExcludeList = {}) -> decltype(auto)
		{
			return Registry.view<Comp...>(ExcludeList);
		}

		template <typename... Comp, typename... Exclude, template<typename...> typename List>
		auto GetView(List<Comp...>, entt::exclude_t<Exclude...> ExcludeList = {}) const -> decltype(auto)
		{
			return Registry.view<Comp const...>(ExcludeList);
		}

		template <typename Resource>
		auto CreateResource(Resource&& Res) -> void
		{
			auto Vi = ResourceStorage.view<Resource>();
			check(Vi.begin() == Vi.end());
			const auto Id = ResourceStorage.create();
			auto& ResComp = ResourceStorage.get_or_emplace<Resource>(Id);
			ResComp = Res;
		}

		template <typename Resource>
		auto GetResource() -> decltype(auto)
		{
			auto Vi = ResourceStorage.view<Resource>();
			check(Vi.begin() != Vi.end());
			return Vi.template get<Resource>(*Vi.begin());
		}

		template <typename Resource>
		auto GetResource() const -> decltype(auto)
		{
			auto Vi = ResourceStorage.view<Resource const>();
			check(Vi.begin() != Vi.end());
			return Vi.template get<Resource const>(*Vi.begin());
		}

		template <typename Resource>
		auto ResourceIsCreated() const -> decltype(auto)
		{
			auto Vi = ResourceStorage.view<Resource const>();
			return Vi.begin() != Vi.end();
		}	

		auto ResourcesAlive() const -> auto
		{
			return ResourceStorage.alive();
		}

		template <typename... Comp, typename... Exclude, template<typename...> typename List>
		auto GetTextInfo(EEntity Entity, FTextBuilder& TextBuilder, List<Comp...>,
		                 entt::exclude_t<Exclude...> ExcludeList = {}) const -> void
		{
			auto Vi = Registry.view<Comp const...>(ExcludeList);
			if (!Vi.contains(Entity))
				return;

			(Vi.template get<Comp const>(Entity).GetTextInfo(TextBuilder), ...);
		}

		template <typename Resource>
		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{
			if (!ResourceIsCreated<Resource>())
				return;

			const auto& Res = GetResource<Resource>();
			Res.GetTextInfo(TextBuilder);
		}
	};

	template <auto Function>
	struct BasicFunctionProcess : entt::process<BasicFunctionProcess<Function>, float>
	{
	public:
		BasicFunctionProcess() = default;

		void update(float DeltaTime, void* Data)
		{
			if (Function(*static_cast<FEnttData*>(Data), DeltaTime) == ESystemStatus::Repeat)
				return;
			succeed();
		}
	};

	template <auto Function>
	struct FunctionProcessWithTimer : entt::process<FunctionProcessWithTimer<Function>, float>
	{
	private:
		float MaxTime;
		float Remaining;
	public:

		FunctionProcessWithTimer(float SetTimer)
			: MaxTime(SetTimer), Remaining(SMALL_NUMBER)
		{
		}

		void update(float DeltaTime, void* Data)
		{
			Remaining -= FMath::Min(Remaining, DeltaTime);
			if (FMath::IsNearlyEqual(Remaining, 0))
			{
				if (Function(*static_cast<FEnttData*>(Data), DeltaTime) == ESystemStatus::Repeat)
				{
					Remaining = MaxTime;
					return;
				}
				succeed();
			}
		}
	};

	struct LambdaProcess : entt::process<LambdaProcess, float>
	{
	private:
		TFunction<ESystemStatus(FEnttData&, float)> Function;

	public:

		LambdaProcess(TFunction<ESystemStatus(FEnttData&, float)> SetFunction)
			: Function(SetFunction)
		{
		}

		void update(float DeltaTime, void* Data)
		{
			if (Function(*static_cast<FEnttData*>(Data), DeltaTime) == ESystemStatus::Repeat)
				return;
			succeed();
		}
	};

	class FEnttWorld final
	{
	private:
		entt::scheduler<float> Scheduler;
		FEnttData Data;

	public:

		template <auto Function>
		void AddSystem()
		{
			Scheduler.attach<BasicFunctionProcess<Function>>();
		}

		template <auto Function>
		void AddSystemWithTimer(float Time)
		{
			Scheduler.attach<FunctionProcessWithTimer<Function>>(Time);
		}

		auto ExecuteSystem(TFunction<ESystemStatus(FEnttData&)> Fun) -> ESystemStatus
		{
			return Fun(Data);
		}

		bool Tick(float DeltaTime)
		{
			Scheduler.update(DeltaTime, &Data);
			return true;
		}
	};
}
